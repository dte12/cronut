<?php

/* Code completed by David Eby, dte12@my.fsu.edu */


require_once('./classes.php');

$storage_file = "./storage.txt";

unset($file_handle);



$csvfile = fopen($storage_file,'r');

while(!feof($csvfile)) {

	    $allfields[] = fgetcsv($csvfile);
	       
}


/* as we saw at the end of class, the $allfields variable above is an array composed of arrays.  Specifically, each array within the bigger array is an array composed of each of the four elements in the csv file (storage.txt) in order.  
 *
 * As such, you should now create an array of OBJECTS, of the class type "subject" (which is the class we have invented for this exercise) So, again, it should be an array of objects, composed of ALL of the objects in our "database" (which, in this case, is simply a flat-file)
 */

$subject_array = array();



foreach ($allfields as $data) {
		$currsub = new subject();
		$currsub->setFirstname($data[0]);
		$currsub->setLastname($data[1]);
		$currsub->setAge($data[2]);
		$currsub->setIncome($data[3]);
		$subject_array[] = $currsub;
		
}

$subjectrecnum = 1;
foreach ($subject_array as $currsub) {
		echo "<br><h3>Subject number $subjectrecnum </h3>";
		echo "<h5>Subject Name:</h5>";
		echo $currsub->getFirstname();
		echo "<br>";
		echo $currsub->getLastname();
		echo "<h5>Code name:</h5>" . $currsub->agetocolor() . " " , $currsub->incometofood();
		$subjectrecnum++;
		
		
}



/* NEXT, after that, create a well formed "report" that returns the following information for EVERY object in the "database," some
thing like
 
 * Subject Name: Firstname Lastname
 * Age: age
 * Income: Income
 * Subject Code : color food
 *
 * Hopefully this is where you begin to see the possibe advantages of OOP; here we can very effectively reuse the code we've already created to provide the subject code (and note, the subject code is not actually stored in the database, and if the coding methodology changes, we're still good) 
 */
 ?>